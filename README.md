## Description
This is a simple scraping Node JS app through which you can get all the future horse 
races from http://betfair.ro, along with all the horses and odds for a specific race.

There are 2 routes:
1. GET /horse-races . This route pulls all the future races organised by date. 
From here the user can pick up any race name and go to route #2.
2. GET /horses-and-odds . This routes returns all the horses and their odds for 
a specific race that the user chooses from route #1.

There is a rate request limit on the /horses-and-odds route as specified in the task. 
In order for the user to make as many requests as possible, he can simply comment out line 30 from app.controller.ts file.

In order to check out the app features, first follow the installation process and proceed
to run the app as listed below.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

App will run on http://localhost:3000/
```

## Test

```bash
# unit tests
$ npm run test

# test coverage
$ npm run test:cov
```
