import { Controller, Get, Inject, Query } from '@nestjs/common';
import { AppService } from './app.service';
import { ConfigService } from '@nestjs/config';
import {
  ApiOperation,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { Throttle } from '@nestjs/throttler';

@ApiTags('Horse Racing Data')
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}
  @Inject(ConfigService)
  private configService: ConfigService;

  @Get('horse-races')
  @ApiOperation({ summary: 'Find all horse races. Structured by date.' })
  @ApiResponse({ status: 200, description: 'Success.' })
  @ApiResponse({ status: 400, description: 'Bad request.' })
  @ApiResponse({ status: 500, description: 'Server error.' })
  loadFutureRaces() {
    return this.appService.getHorseRacesByDate();
  }

  // Set request rate limit to 1 every 60 seconds
  @Throttle(1, 60)
  //
  @Get('horses-and-odds')
  @ApiQuery({
    name: 'race_name',
    example: 'Melbourne Cup',
    type: String,
    description: 'A parameter for race name.',
    required: true,
  })
  @ApiOperation({ summary: 'Get horse and odds by race name.' })
  @ApiResponse({ status: 200, description: 'Success.' })
  @ApiResponse({ status: 400, description: 'Bad request.' })
  @ApiResponse({ status: 500, description: 'Server error.' })
  getHorseAndOddByRace(@Query('race_name') race_name: string) {
    let response;
    try {
      response = this.appService.findHorseAndOddsByRaceName(race_name);
    } catch (e) {
      throw new Error(e);
    }
    return response;
  }
}
