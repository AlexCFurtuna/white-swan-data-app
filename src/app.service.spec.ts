import { AppService } from './app.service';
import * as fs from 'fs';
import axios from 'axios';
jest.mock('axios');
const mockedAxios = axios as jest.Mocked<typeof axios>;
describe('AppService', () => {
  let appService: AppService;
  const getHorseRaceByDateMock = [
    {
      date: '16 martie 2023',
      races: [
        'Turners Novices Chase - Non Runner Money Back',
        'Pertemps Network Final - Non Runner Money Back',
        'Ryanair Chase - Non Runner Money Back',
        'Paddy Power Stayers Hurdle - Non Runner Money Back',
        'Magners Plate Handicap Chase - Non Runner Money Back',
        'Mares Novices Hurdle - Non Runner Money Back',
        'Kim Muir Challenge Cup - Non Runner Money Back',
      ],
    },
    {
      date: '17 martie 2023',
      races: [
        'Triumph Hurdle - Non Runner Money Back',
        'County Hurdle - Non Runner Money Back',
        'Albert Bartlett Novices Hurdle - Non Runner Money Back',
        'Gold Cup - Non Runner Money Back',
        "St James's Place Festival Hunters Chase - Non Runner Money Back",
        'Mrs Paddy Power Mares Chase - Non Runner Money Back',
        'Martin Pipe Handicap Hurdle - Non Runner Money Back',
      ],
    },
    {
      date: '01 aprilie 2023',
      races: ['The Lincoln Handicap'],
    },
    {
      date: '15 aprilie 2023',
      races: ['Grand National'],
    },
    {
      date: '06 mai 2023',
      races: ['2000 Guineas'],
    },
    {
      date: '07 mai 2023',
      races: ['1000 Guineas'],
    },
    {
      date: '02 iunie 2023',
      races: ['The Oaks'],
    },
    {
      date: '03 iunie 2023',
      races: ['The Derby'],
    },
    {
      date: '23 iunie 2023',
      races: ['Cupa Commonwealth-ului'],
    },
    {
      date: '26 decembrie 2023',
      races: ['King George VI Chase'],
    },
    {
      date: '12 martie 2024',
      races: [
        '2024 Supreme Novices Hurdle',
        '2024 Arkle Chase',
        '2024 Champion Hurdle',
        '2024 Mares Hurdle',
        '2024 National Hunt Chase',
      ],
    },
    {
      date: '13 martie 2024',
      races: [
        '2024 Ballymore Novices Hurdle',
        '2024 Brown Advisory Novices Chase',
        '2024 Champion Chase',
      ],
    },
    {
      date: '14 martie 2024',
      races: [
        '2024 Turners Novices Chase',
        '2024 Ryanair Chase',
        '2024 Paddy Power Stayers Hurdle',
        '2024 Mares Novices Hurdle',
      ],
    },
    {
      date: '15 martie 2024',
      races: [
        '2024 Albert Bartlett Novices Hurdle',
        '2024 Gold Cup',
        '2024 Mrs Paddy Power Mares Chase',
      ],
    },
    {
      date: '25 martie 2023',
      races: [
        'Al Quoz Sprint',
        'Dubai Gold Cup',
        'Golden Shaheen',
        'Dubai Turf',
        'UAE Derby',
        'Godolphin Mile',
        'Dubai Sheema Classic',
        'Dubai World Cup',
      ],
    },
    {
      date: '07 mai 2023',
      races: ['French 1000 Guineas', 'French 2000 Guineas'],
    },
    {
      date: '04 iunie 2023',
      races: ['Prix Du Jockey Club'],
    },
    {
      date: '18 iunie 2023',
      races: ['Prix Diane'],
    },
    {
      date: '01 octombrie 2023',
      races: ["Prix d l'Arc de Triomphe"],
    },
    {
      date: '21 octombrie 2023',
      races: ['Caulfield Cup'],
    },
    {
      date: '28 octombrie 2023',
      races: ['Cox Plate'],
    },
    {
      date: '02 noiembrie 2023',
      races: ['Melbourne Cup'],
    },
  ];
  const horseNameAndOddMock = [
    {
      horse_name: 'Francesco Guardi',
      horse_odd: '13.0',
    },
    {
      horse_name: 'Soulcombe',
      horse_odd: '13.0',
    },
    {
      horse_name: 'White Marlin',
      horse_odd: '13.0',
    },
    {
      horse_name: 'Brayden Star',
      horse_odd: '17.0',
    },
    {
      horse_name: 'Gold Trip',
      horse_odd: '17.0',
    },
    {
      horse_name: 'Hoo Ya Mal',
      horse_odd: '17.0',
    },
    {
      horse_name: 'Cleveland',
      horse_odd: '21.0',
    },
    {
      horse_name: 'Deauville Legend',
      horse_odd: '21.0',
    },
    {
      horse_name: 'Durston',
      horse_odd: '21.0',
    },
    {
      horse_name: 'Emissary',
      horse_odd: '21.0',
    },
    {
      horse_name: 'Fancy Man',
      horse_odd: '21.0',
    },
    {
      horse_name: 'Loft',
      horse_odd: '21.0',
    },
    {
      horse_name: 'Waterville',
      horse_odd: '21.0',
    },
    {
      horse_name: 'Lunar Flare',
      horse_odd: '26.0',
    },
    {
      horse_name: 'Spanish Mission',
      horse_odd: '26.0',
    },
    {
      horse_name: 'Vow And Declare',
      horse_odd: '26.0',
    },
    {
      horse_name: 'El Bodegon',
      horse_odd: '34.0',
    },
    {
      horse_name: 'High Emocean',
      horse_odd: '34.0',
    },
    {
      horse_name: 'Il Paradiso',
      horse_odd: '34.0',
    },
    {
      horse_name: 'Benaud',
      horse_odd: '41.0',
    },
    {
      horse_name: 'Milford',
      horse_odd: '41.0',
    },
    {
      horse_name: 'Serpentine',
      horse_odd: '51.0',
    },
  ];

  const htmlContentOne = fs.readFileSync(
    __dirname + '/html-content-mocks/get-initial-content.txt',
    'utf-8',
  );
  const htmlContentTwo = fs.readFileSync(
    __dirname + '/html-content-mocks/get-content-for-future-races.txt',
    'utf-8',
  );
  const htmlContentThree = fs.readFileSync(
    __dirname + '/html-content-mocks/get-found-future-race-content.txt',
    'utf-8',
  );
  describe('getHorseRacesByDate', () => {
    beforeEach(async () => {
      appService = new AppService();
    });
    it('should return data', async () => {
      jest.spyOn(appService, 'getHorseRacesByDate').mockReturnValueOnce(
        Promise.resolve(
          getHorseRaceByDateMock as unknown as Array<{
            date: string;
            races: [];
          }>,
        ),
      );
      const getRaces = await appService.getHorseRacesByDate();
      expect(getRaces).toEqual(getHorseRaceByDateMock);
    });
    it('should return empty array', async () => {
      jest
        .spyOn(appService, 'getHorseRacesByDate')
        .mockReturnValueOnce(Promise.resolve([]));
      const getRaces = await appService.getHorseRacesByDate();
      expect(getRaces).toEqual([]);
    });
    it('should return future races', async () => {
      mockedAxios.get.mockResolvedValue({ data: htmlContentOne });
      const getRaces = await appService.getHorseRacesByDate();
      expect(getRaces).toEqual(getHorseRaceByDateMock);
    });
    it('should return empty array', async () => {
      mockedAxios.get.mockResolvedValue({ data: '' });
      const getRaces = await appService.getHorseRacesByDate();
      expect(getRaces).toEqual([]);
    });
  });
  describe('findHorseAndOddsByRaceName', () => {
    beforeEach(async () => {
      appService = new AppService();
    });
    it('should return data', async () => {
      jest.spyOn(appService, 'findHorseAndOddsByRaceName').mockReturnValueOnce(
        Promise.resolve(
          horseNameAndOddMock as unknown as Array<{
            horse_name: string;
            horse_odd: string;
          }>,
        ),
      );
      const getRaces = await appService.findHorseAndOddsByRaceName('');
      expect(getRaces).toEqual(horseNameAndOddMock);
    });
    it('should throw not found exception', async () => {
      jest
        .spyOn(appService, 'findHorseAndOddsByRaceName')
        .mockRejectedValue('No race found with the name of: test');
      try {
        await appService.findHorseAndOddsByRaceName('');
      } catch (e) {
        expect(e).toEqual('No race found with the name of: test');
      }
    });
    it('should return horses and odds', async () => {
      jest.spyOn(appService, 'getHorseRacesByDate').mockReturnValueOnce(
        Promise.resolve(
          getHorseRaceByDateMock as unknown as Array<{
            date: string;
            races: [];
          }>,
        ),
      );
      mockedAxios.get
        .mockResolvedValueOnce({ data: htmlContentTwo })
        .mockResolvedValueOnce({ data: htmlContentThree });
      const getRaces = await appService.findHorseAndOddsByRaceName(
        'Melbourne Cup',
      );
      expect(getRaces).toEqual(horseNameAndOddMock);
    });
    it('should throw not found exception', async () => {
      jest.spyOn(appService, 'getHorseRacesByDate').mockReturnValueOnce(
        Promise.resolve(
          getHorseRaceByDateMock as unknown as Array<{
            date: string;
            races: [];
          }>,
        ),
      );
      mockedAxios.get
        .mockResolvedValueOnce({ data: htmlContentTwo })
        .mockResolvedValueOnce({ data: htmlContentThree });
      try {
        await appService.findHorseAndOddsByRaceName('not_a_race_name');
      } catch (e) {
        expect(e.message).toEqual(
          'No race found with the name of: not_a_race_name',
        );
        expect(e.name).toEqual('NotFoundException');
      }
    });
  });
});
