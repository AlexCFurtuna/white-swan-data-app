import { Injectable, NotFoundException } from '@nestjs/common';
import axios from 'axios';
import * as cheerio from 'cheerio';
@Injectable()
export class AppService {
  // Get all the races by location and by date
  async getHorseRacesByDate(): Promise<
    Array<{ date: string; races: [] }> | []
  > {
    const horseRacesByDate = [];
    const url =
      'https://www.betfair.ro/sport/horse-racing?action=loadFutureRacing';
    const { data } = await this.getContent(url);
    const $ = cheerio.load(data);
    const getFutureRaceEvents = $(
      '#zone-main .event p, #zone-main .event.market-list',
    ).each((index, value) => {
      const horseRace = {
        date: '',
        races: [],
      };
      if (value.next) {
        horseRace.date = value['children'][0]['data'];
        value.next['children'].forEach((child) => {
          if (child['children']) {
            horseRace.races.push(
              child['children'][0]['children'][1]['children'][0]['children'][0]
                .data,
            );
          }
        });
      }
      horseRacesByDate.push(horseRace);
    });
    return horseRacesByDate;
  }

  async findHorseAndOddsByRaceName(
    raceName: string,
  ): Promise<Array<{ horse_name: string; horse_odd: string }>> {
    let url = 'https://www.betfair.ro';
    const path = '/sport/horse-racing?action=loadFutureRacing';
    let { data } = await this.getContent(url, path);
    let $ = cheerio.load(data);
    for (const horseRaceByDate of await this.getHorseRacesByDate()) {
      for (const race of horseRaceByDate.races) {
        if (race == raceName) {
          const horsesAndOdds = [];
          const raceLinks = $('#zone-main .event ul li');
          for (const raceLink of raceLinks) {
            if (
              raceLink['children'][0]['attribs']['data-galabel'] == raceName
            ) {
              // Build horse race link
              url = url + raceLink['children'][0]['attribs']['href'];
              break;
            }
          }
          // Get race HTML
          data = await this.getContent(url);
          $ = cheerio.load(data.data);
          const horsesAndOddsHtml = $(
            '#zone-main .outright-runner-list .runner-info-name > div',
          );
          for (const horseAndOds of horsesAndOddsHtml) {
            horsesAndOdds.push({
              horse_name: horseAndOds['attribs']['title'],
              horse_odd:
                horseAndOds['children'][0]['parent']['parent']['parent'][
                  'children'
                ][0]['children'][0]['children'][0]['children'][0]['data'],
            });
          }
          return horsesAndOdds;
        }
      }
    }
    throw new NotFoundException(
      `No race found with the name of: ${raceName}`,
      'AppService.findHorseAndOddsByRaceName',
    );
  }

  async getContent(baseUrl: string, path?: string) {
    let url = baseUrl;
    if (path) {
      url = baseUrl + path;
    }
    return await axios.get(url);
  }
}
